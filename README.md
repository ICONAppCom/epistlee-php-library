##Epistlee PHP Library

*Library Version 2.0.0 for Epislee version 3+*

**Epistlee PHP Library** makes the work easier for you by doing all the request calls to the Epistlee API for you. All you need to do is give your Username.

Download the latest Library from the [Downloads](https://bitbucket.org/ICONAppCom/epistlee-php-library/downloads) sections. And see the [Wiki](https://bitbucket.org/ICONAppCom/epistlee-php-library/wiki/Home) section on how to use the Library.