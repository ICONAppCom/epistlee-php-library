<?php 
	
	/*
		Epistlee Library (PHP).
		Library Version 2.0.0 for Epistlee version 3+
		Copyright ICON AppCom 2013.
		http://iconappcom.in
		http://epistlee.com
		
	*/
	
	class Epistlee
	{
		private $usera;
		private $passb;
		
		/*
			Set the base function
		*/
		function __construct($username, $password)
		{
			$this->usera = $username;
			$this->passb = $password;
		}
		
		/*
			Get the User's blog entries (Optional, set the Page value)
		*/
		function get_entries($page=null)
		{
			$user = $this->usera;
			$pass = $this->passb;
		
			//Make the Request...
			$ch = curl_init();
			
			//Get entries with page specified or not...
			if($page != null)
			{
				curl_setopt($ch, CURLOPT_URL, "http://{$user}.epistlee.com/?p=" . $page);
			}
			else
			{
				curl_setopt($ch, CURLOPT_URL, "http://{$user}.epistlee.com/");
			}
			
			//Do a GET request
			curl_setopt($ch, CURLOPT_HTTPGET, true);
			
			//Set the Authorization header...
			curl_setopt($ch, CURLOPT_USERPWD, "{$user}:{$pass}");
			
			//Set the Accept header to application/json
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
			
			//Get back the response body...
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			
			//Execute the request...
			$results = curl_exec($ch);
			if($results === false)
			{
				return curl_error($results);
			}
			else
			{
				$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				if($http_status == "200")
				{
					$ff = json_decode($results);
					return $ff;
				}
				elseif($http_status == "400")
				{
					return $results;
				}
			
			}
			
			//Close the CURL...
			curl_close($ch);
		}
		
		/*	
			Get a specified entry via its Base URL...
		*/
		function get_entry($baseurl)
		{
			$user = $this->usera;
			$pass = $this->passb;
		
			//Make the Request...
			$ch = curl_init();
			
			//Get entry with specified base URL
			curl_setopt($ch, CURLOPT_URL, "http://{$user}.epistlee.com/{$baseurl}");
			
			//Do a GET request
			curl_setopt($ch, CURLOPT_HTTPGET, true);
			
			//Set the Authorization header...
			curl_setopt($ch, CURLOPT_USERPWD, "{$user}:{$pass}");
			
			//Set the Accept header to application/json
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
			
			//Get back the response body...
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			
			//Execute the request...
			$results = curl_exec($ch);
			if($results === false)
			{
				return curl_error($results);
			}
			else
			{
				$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				if($http_status == "200")
				{
					$ff = json_decode($results);
					return $ff;
				}
				elseif($http_status == "400")
				{
					return $results;
				}
			
			}
			
			//Close the CURL...
			curl_close($ch);
		}
		
		/*	
			Get user info...
		*/
		function get_user_info()
		{
			$user = $this->usera;
			$pass = $this->passb;
		
			//Make the Request...
			$ch = curl_init();
			
			//To the URL /a
			curl_setopt($ch, CURLOPT_URL, "http://epistlee.com/a");
			
			//Do a GET request
			curl_setopt($ch, CURLOPT_HTTPGET, true);
			
			//Set the Authorization header...
			curl_setopt($ch, CURLOPT_USERPWD, "{$user}:{$pass}");
			
			//Set the Accept header to application/json
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
			
			//Get back the response body...
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			
			//Execute the request...
			$results = curl_exec($ch);
			if($results === false)
			{
				return curl_error($results);
			}
			else
			{
				$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				if($http_status == "200")
				{
					$ff = json_decode($results);
					return $ff;
				}
				elseif($http_status == "400")
				{
					return $results;
				}
			
			}
			
			//Close the CURL...
			curl_close($ch);
		}
	}
	
?>